import React from "react";
import { Link } from "react-router-dom";

import Button from "../../components/Button";

export function HomePage(props) {
  return (
    <main className="mainBox">
      <img />
      <h1>{props.name}</h1>
      <h3>{props.occup}</h3>
      <p>
        Lorem ipsum dolor sit amet,
        consectetuer adipiscing elit.
        Aenean commodo ligula eget dolor.
        Aenean massa. Cum sociis natoque
      </p>
      <Link to="innerpage">
        <Button text="Know more" />
      </Link>
    </main>
  );
}

export default HomePage;
