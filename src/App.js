import React from 'react';
import { Routes, Route } from 'react-router-dom';

import HomePage from './pages/home/HomePage';
import InnerPage from './pages/inner/InnerPage';

import './App.css';

function App() {
  return (
    <Routes> 
      <Route exact path="/" element={<HomePage name="Krystsina Valushka" occup="Front End Developer"/>} />
      <Route exact path="/innerPage" element={<InnerPage />} />
    </Routes>
  );
}

export default App;
